<div align="center">
    <img src="https://user-images.githubusercontent.com/61925890/177252974-f25c5ad2-920b-48ae-aefe-aba788364de8.gif" width="200">
</div>

# ComputerCraft GitLab Cloner
This script allows you to clone a GitLab repository to your [ComputerCraft computer/turtle](https://tweaked.cc). It uses the [GitLab API](https://docs.gitlab.com/ee/api/) to fetch the project tree and then manually fetch the raw file contents. Although GitLab does allow you to download a zipped version of a repository in one request, I have not found a practical way to unzip all the files in pure Lua in ComputerCraft.

> Note:
> The script is limited to repositories with a maximum of ~30,000 files.

## Install
Gitlab cloner can be installed using the `wget` program to download the bundled Lua file from the latest release:

```
wget https://gitlab.com/api/v4/projects/37453574/releases/permalink/latest/downloads/out/gitlab-cloner.lua
```

This will download `gitlab-cloner.lua` and place it in your current directory.


## Usage
The script takes the ID of the project as an argument and uses unix-style flags to accept options.

The most basic usage is like so:

`gitlab-cloner <repo_id>`

You can also define glob patterns to ignore certain files when cloning repositories. This can be done in a `.cloneignore` file in the same directory as the main `gitlab-cloner.lua` file. Here is an example `.cloneignore` file:

```
.vscode/*
tests/**/*.lua
ignore-me.lua
*.zip
```

> Note:
> To match the contents of a directory, the glob pattern must end with `/*` like with the `.vscode/*` example above.

## Arguments

### `id`
**Required**

This should be the [ID](https://stackoverflow.com/a/53126068 "Where do I find the project ID?") of your GitLab project

### `--branch`
This flag allows you to specify which branch of the repo you would like to clone.

`gitlab-cloner <repo_id> --branch=<branch_name>`

### `--key`
`gitlab-cloner <repo_id> --key=<access_token>`
A [gitlab access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) for your account. This allows you to clone private repos and increases the amount of files that can be requested per minute before being rate limited.

The key should have the `read_api` and `read_repository` permissions in order to function.
![permissions](https://user-images.githubusercontent.com/61925890/190555673-e192f48d-3910-43e5-bbba-ffa7a7009708.png)

### `--skip-delay`
Skips the `Proceeding in X...` countdown before a clone begins.

## Examples
Clone this repo's `testing` branch:

`gitlab-cloner 37453574 --branch=testing`

Clone this repo's `development` branch and provide an access token.

`gitlab-cloner 37453574 --branch=development --key=glpat-xxxxxxxx-xxxxxxxxxxx`

## Credit
- [lua-glob-pattern](https://github.com/davidm/lua-glob-pattern) by [davidm](https://github.com/davidm)
- [argparse](https://github.com/mpeterv/argparse) by [mpeterv](https://github.com/mpeterv)
