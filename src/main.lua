package.path = package.path .. ";lib/?.lua;lib/?/init.lua"

local logging = require("ccLogging")
local FileHandler = require("ccLogging.classes.FileHandler")

--Set up logging
local logger = logging.globalLogger
logger.handlers[1].logLevel = logging.config.levels.Trace
logger:registerHandler(FileHandler.new("/logs/gitlab-cloner.log"))

local function errorHandler(err)
	logger:error(err)
end

local pretty = require("cc.pretty")
local printing = require("util.printing")

local argparse = require("argparse")
local gitlab = require("api.gitlab")
local download = require("jobs.download")
local luaglob = require("lua-glob-pattern")

local arguments = { ... }
---Patterns to use for ignoring files
local ignorePatterns = {}

term.clear()
term.setCursorPos(1, 3)

printing.printLogo()

settings.define("http.max_concurrent_http_requests", {
	description = "The maximum number of http requests that can be performed at once",
	type = "number",
	default = 16,
})

---@type argparse.Parser
local argParser = argparse("gitlab-cloner", "gitlab-cloner <projectID> -p=?.lua")
argParser:argument("projectID", "ID of the project to clone")
argParser:option("-p --path", "Lua require path")
argParser:option("--branch", "Branch to clone")
argParser:option("--key", "GitLab personal access token", nil)
argParser:flag("--skip-delay", "Skip the countdown before continuing to clone")


local success, args = argParser:pparse(arguments)
if not success then
	printError(args)
	return
end

if args.key then
	logger:debug("Access token: " .. pretty.text(string.rep("*", #args.key), colors.orange))
	gitlab.authenticate(args.key)
else
	print(
		"Tip: Use a Gitlab Personal Access Token with the --key=KEY flag to increase the rate limit for large repos"
	)
end

local exists = fs.exists(".cloneignore")
if exists then
	local file, err = io.open(".cloneignore", "r")
	if not file then
		logger:error(err)
		return
	end

	for line in file:lines("l") do
		table.insert(ignorePatterns, luaglob.globtopattern(line))
	end
end

logger:debug("Ignore patterns: " .. table.concat(ignorePatterns, ", "))


local success, projectInfo = pcall(gitlab.projects.getProject, args.projectID)
if not success then
	logger:error("Failed to get project info")
	return
end

logger:debug("Received project info")

local projectNameEscaped = string.gsub(projectInfo.name, " ", "-")
local projectPath = string.gsub(projectInfo.name_with_namespace, " / ", "/")
local branch = args.branch or projectInfo.default_branch

logger:info("Cloning " .. pretty.text(branch, colors.lime) .. " from " .. pretty.text(projectPath, colors.magenta))

if not args.skip_delay then
	term.scroll(1)
	local _, y = term.getCursorPos()

	for i = 5, 1, -1 do
		print("Proceeding in " .. i .. "...")
		sleep(1)
		term.setCursorPos(1, y)
	end
end

local _, tree = xpcall(gitlab.repositories.getTree, errorHandler, args.projectID, branch)
if not tree then
	return
end

if #tree == 0 then
	logger:error("Repository is empty")
	return
end

local progress = { completed = 0, inProgress = 0, total = 0, failures = 0 }

local function updateProgressBar()
	local w, h = term.getSize()
	local _, y = term.getCursorPos()

	term.setCursorPos(1, 1)

	local barSegmentWidth = w / progress.total
	local completeBarWidth = math.ceil(barSegmentWidth * progress.completed)
	local inProgressBarWidth = math.ceil(barSegmentWidth * progress.inProgress)

	term.setBackgroundColor(colors.green)
	term.write(string.rep(" ", completeBarWidth))
	term.setBackgroundColor(colors.yellow)
	term.write(string.rep(" ", inProgressBarWidth))
	term.setBackgroundColor(colors.lightGray)
	term.write(string.rep(" ", w - completeBarWidth - inProgressBarWidth))

	term.setBackgroundColor(colors.black)
	term.setCursorPos(1, y)
end

---@type string[]
local files = {}

-- TODO: add .cloneignore that uses glob pattern to ignore certain files and folders in the target repo

for _, treeItem in pairs(tree) do
	local ignore = false

	if treeItem.type == "blob" then
		for _, pattern in ipairs(ignorePatterns) do
			if string.match(treeItem.path, pattern) then
				ignore = true
				break
			end
		end

		if ignore then
			logger:debug("Ignoring " .. treeItem.path)
		else
			table.insert(files, treeItem.path)
		end
	end
end

logger:debug("Repository contains " .. #files .. " files")

progress.total = #files

---@type jobs.Download[]
local threads = {}

for i = 1, math.min(#files, settings.get("http.max_concurrent_http_requests")) do
	threads[i] = download.new(args.projectID, branch)
	threads[i]:initThread()
end

local threadManLog = logger:createChild("Thread Manager")

threadManLog:debug("Created " .. #threads .. " threads")

---@type {[1]: event, [integer]: any}
local event = {}

while progress.completed < progress.total do
	local rateLimited = 0

	for i, thread in ipairs(threads) do
		if thread.rateLimited then
			rateLimited = rateLimited + 1
		end

		if thread:status() == "suspended" then
			if thread.filepath then
				if thread.attempts == 0 then
					progress.inProgress = progress.inProgress + 1
				end

				local success, data = coroutine.resume(thread.thread, table.unpack(event))

				if success then
					if data == true then
						progress.completed = progress.completed + 1
						progress.inProgress = progress.inProgress - 1

						if #files > 0 then
							thread:initThread()
							thread.filepath = nil
						end
					else
						if not thread.rateLimited and data == "timer" then
							rateLimited = rateLimited + 1
							thread.rateLimited = true
						end
					end
				else
					threadManLog:error("Thread " .. pretty.text(tostring(i), colors.pink) .. " failed")
					thread:initThread()
					thread.filepath = nil
					thread.installPath = nil
					progress.failures = progress.failures + 1
					progress.completed = progress.completed + 1
					progress.inProgress = progress.inProgress - 1
				end
			else
				local file = table.remove(files)

				if file then
					thread:setFile(file, fs.combine(projectNameEscaped, file))
				end
			end
		end

		updateProgressBar()
	end

	---If a thread is waiting for an event...
	if progress.inProgress > 0 or rateLimited > 0 then
		event = { os.pullEvent() }
	end
end

term.scroll(1)
if progress.failures > 0 then
	term.setTextColor(colors.black)
	term.setBackgroundColor(colors.yellow)
	print(progress.failures .. " files failed to download")
else
	term.setTextColor(colors.white)
	term.setBackgroundColor(colors.green)
	print("Clone Completed")
end

term.setBackgroundColor(colors.black)
term.clearLine()
term.scroll(1)

updateProgressBar()
