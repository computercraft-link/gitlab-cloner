local config       = require("api.gitlab.config")
local repositories = require("api.gitlab.repositories")
local projects     = require("api.gitlab.projects")

---@alias gitlab.ProjectID integer|string Project ID or namespace path


local gitlab = {
    projects = projects,
    repositories = repositories
}

---@param key string
function gitlab.authenticate(key)
    config.headers["Authorization"] = "Bearer " .. key
end

return gitlab
