local config      = require("api.gitlab.config")
local httpTweaked = require("lib.httpTweaked")
local URL         = require("lib.httpTweaked.classes.URL")


local repositories = {
    _ENDPOINT = "/projects/:id/repository"
}

---@class gitlab.repositories.treeItem
---@field id string
---@field name string
---@field type string
---@field path string
---@field mode integer

---Get the file tree of a repository
---@param projectID gitlab.ProjectID
---@param ref string Branch to get tree of
---@return gitlab.repositories.treeItem[]
---@nodiscard
function repositories.getTree(projectID, ref)
    local tree = {}

    local url = URL.new(httpTweaked.fillParameters(config._ENDPOINT .. repositories._ENDPOINT .. "/tree",
        { id = projectID }))
    url:updateSearch({ ref = ref, recursive = true, pagination = "keyset", per_page = 100 })

    ---@param url httpTweaked.URL
    local function collect(url)
        local success, response = pcall(httpTweaked.request, { url = tostring(url), headers = config.headers })

        if success then
            for _, v in pairs(response.data) do
                table.insert(tree, v)
            end

            if response.headers["link"] then
                local _, _, nextLink = string.find(response.headers["link"], '<([%w:/?&=._]+)>; rel="next"')
                if nextLink then
                    return collect(nextLink)
                end
            end
        else
            error(response)
        end
    end

    collect(url)

    return tree
end

---Get a file's raw contents
---@param projectID gitlab.ProjectID
---@param path string Full path to the file in the repository
---@param ref string? Branch to get the file from
---@return string contents
---@nodiscard
function repositories.getRawFile(projectID, path, ref)
    local url = URL.new(
        httpTweaked.fillParameters(
            config._ENDPOINT .. repositories._ENDPOINT .. "/files/:file_path/raw",
            { id = projectID, file_path = httpTweaked.encodeURL(path):gsub("%%", "%%%%") }
        )
    )

    if ref then
        url:updateSearch({ ref = ref })
    end

    local success, response = pcall(httpTweaked.request,
        { url = tostring(url), method = "GET", headers = config.headers })

    if success then
        return response.data
    else
        error(response)
    end
end

return repositories
