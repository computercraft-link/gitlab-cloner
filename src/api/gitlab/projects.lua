local config      = require("api.gitlab.config")
local httpTweaked = require "lib.httpTweaked"
local URL         = require "lib.httpTweaked.classes.URL"


local projects = {
    _ENDPOINT = "/projects"
}

---@param projectID integer
---@return {name: string, name_with_namespace: string, default_branch: string}
---@nodiscard
function projects.getProject(projectID)
    local url = URL.new(httpTweaked.fillParameters(config._ENDPOINT .. projects._ENDPOINT .. "/:id", { id = projectID }))

    return httpTweaked.request({ url = tostring(url), method = "GET", headers = config.headers }).data
end

return projects
