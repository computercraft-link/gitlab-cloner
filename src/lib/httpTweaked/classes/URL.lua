---@class httpTweaked.URL
---@field hash string
---@field hostname string
---@field pathname string
---@field port string
---@field protocol string
---@field search string
---@overload fun(url: string): httpTweaked.URL
local URL = {}

---Create a new URL
---@param url string
---@return httpTweaked.URL
function URL.new(url)
    local self = {}

    local mt = {
        __index = URL,

        __tostring = function(obj)
            local url = obj.protocol .. "://" .. obj.hostname
            if obj.port then
                url = url .. ":" .. obj.port
            end
            url = url .. obj.pathname
            if obj.search then
                url = url .. obj.search
            end
            if obj.hash then
                url = url .. obj.hash
            end

            return url
        end
    }

    self = setmetatable(self, mt)

    local success, err = pcall(self._parseURL, self, url)
    if not success then
        error(err, 2)
    end

    return self
end

---Parse a url into its segments
---@private
---@param url string
function URL:_parseURL(url)
    local protocol, hostname, port, pathname, search, hash = url:match(
        "^(%a+):[\\/]*([%w%.%-]+):?(%d*)([^#%?]*)(%??[^#]*)(#?.*)$")

    if not protocol or not hostname then
        error("Invalid URL: " .. url, 2)
    end

    self.protocol = protocol
    self.hostname = hostname
    self.port = port ~= "" and port or nil
    self.pathname = pathname
    self.hash = hash

    local searchParams = {}
    for key, value in string.gmatch(search, "([^&]+)=([^&]+)") do
        searchParams[key] = value
    end

    self:updateSearch(searchParams)
end

---Update the `search` field using the `searchParams` field
function URL:updateSearch(params)
    self.search = "?"

    for key, value in pairs(params) do
        self.search = self.search .. string.format("%s=%s&", key, tostring(value))
    end
    self.search = string.sub(self.search, 1, #self.search - 1)

    if self.search == "?" then
        self.search = nil
    end
end

return URL
