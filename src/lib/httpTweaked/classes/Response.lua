local pretty = require("cc.pretty")

local STATUSES = {
	[100] = "Continue",
	[101] = "Switching Protocols",
	[102] = "Processing",
	[103] = "Early Hints",
	[200] = "OK",
	[201] = "Created",
	[202] = "Accepted",
	[203] = "Non-Authoritative Information",
	[204] = "No Content",
	[205] = "Reset Content",
	[206] = "Partial Content",
	[207] = "Multi-Status",
	[208] = "Already Reported",
	[226] = "IM Used",
	[300] = "Multiple Choices",
	[301] = "Moved Permanently",
	[302] = "Found",
	[303] = "See Other",
	[304] = "Not Modified",
	[307] = "Temporary Redirect",
	[308] = "Permanent Redirect",
	[400] = "Bad Request",
	[401] = "Unauthorized",
	[402] = "Payment Required",
	[403] = "Forbidden",
	[404] = "Not Found",
	[405] = "Method Not Allowed",
	[406] = "Not Acceptable",
	[407] = "Proxy Authentication Required",
	[408] = "Request Timeout",
	[409] = "Conflict",
	[410] = "Gone",
	[411] = "Length Required",
	[412] = "Precondition Failed",
	[413] = "Content Too Large",
	[414] = "URI Too Long",
	[415] = "Unsupported Media Type",
	[416] = "Range Not Satisfiable",
	[417] = "Expectation Failed",
	[418] = "I'm a teapot",
	[421] = "Misdirected Request",
	[422] = "Unprocessable Content",
	[423] = "Locked",
	[424] = "Failed Dependency",
	[425] = "Too Early",
	[426] = "Upgrade Required",
	[428] = "Precondition Required",
	[429] = "Too Many Requests",
	[431] = "Request Header Fields Too Large",
	[451] = "Unavailable For Legal Reasons",
	[500] = "Internal Server Error",
	[501] = "Not Implemented",
	[502] = "Bad Gateway",
	[503] = "Service Unavailable",
	[504] = "Gateway Timeout",
	[505] = "HTTP Version Not Supported",
	[506] = "Variant Also Negotiates",
	[507] = "Insufficient Storage",
	[508] = "Loop Detected",
	[510] = "Not Extended",
	[511] = "Network Authentication Required",
}

---@class httpTweaked.Response
---@field ok boolean Whether the response is ok (a `2xx` response) or an error
---@field code integer Status code
---@field status string Status text
---@field data any Response data from the server
---@field headers table<string, string> HTTP response headers
---@field request HTTP_REQUEST The request that generated this response
---@field duration integer Number of ms taken to fullfil request
local Response = {
	__type = "Response",
}

Response.__index = Response

---Create a new response object from a CC:Tweaked `Response`
---@param request HTTP_REQUEST The request that generated this response
---@param response Response The CC:Tweaked `Response`
---@param duration integer The number of ms taken to fullfil this request
---@return httpTweaked.Response
function Response.new(request, response, duration)
	local self = setmetatable({}, Response)

	self.code, self.status = response.getResponseCode()
	self.headers = response.getResponseHeaders()
	self.ok = self.code > 199 and self.code < 300
	self.status = self.status or STATUSES[self.code] or "???"
	self.request = request
	self.duration = duration

	local content = response.readAll()

	self.data = content
	self:unserializeData()

	return self
end

function Response:unserializeData()
	local contentType = self.headers["Content-Type"]

	if string.find(contentType, "application/json") then
		local data, err = textutils.unserializeJSON(self.data --[[@as string]])
		if not data then
			error("Failed to parse JSON: " .. err)
		end
		self.data = data
	end
end

---Get a prettified response message
function Response:pretty()
	return pretty.concat(
		pretty.text(string.format("%d %s:", self.code, self.status), self.ok and colors.green or colors.red),
		pretty.space,
		pretty.text(self.request.url, colors.white),
		self.ok == false and pretty.concat(pretty.space_line, pretty.pretty(self.data)) or "",
		pretty.space,
		pretty.text(string.format("(%dms)", self.duration),
			self.duration > 1000 and colors.red or self.duration > 500 and colors.yellow or colors.green)
	)
end

return Response
