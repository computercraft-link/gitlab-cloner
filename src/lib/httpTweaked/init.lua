local Response = require("lib.httpTweaked.classes.Response")
local logging = require("ccLogging")

local logger = logging.globalLogger:createChild("httpTweaked")

local httpTweaked = {}

---Encode a string to be safe for use in or as a URL
---@param str string
---@return string
function httpTweaked.encodeURL(str)
	if str then
		str = string.gsub(str, "\n", "\r\n")
		str = string.gsub(str, "([^%w ])",
			function(c) return string.format("%%%02X", string.byte(c)) end
		)
		str = string.gsub(str, " ", "%%20")
	end
	return str
end

---Turn a key-value pair table into a query string
---@param queries table<string, string|boolean|number>
---@return string queryString
---@nodiscard
function httpTweaked.buildQueryString(queries)
	local str = "?"

	for k, v in pairs(queries) do
		str = str .. k .. "=" .. textutils.urlEncode(tostring(v)) .. "&"
	end

	return string.sub(str, 1, #str - 1)
end

---Given a url with parameters, replace the parameters with provided values
---@param url string The url with params. (e.g. `https://example.com/user/:id`)
---@param params table<string, any> Key value pairs where the key is the parameter name from the URL
---@return string url
function httpTweaked.fillParameters(url, params)
	for key, value in pairs(params) do
		url = string.gsub(url, ":" .. key, value)
	end

	local start, nd, param
	repeat
		start, nd, param = string.find(url, ":([%w%-_]+)")
		if start ~= nil then
			---@cast nd integer
			if params[param] == nil then
				error("Value not provided for URL parameter: " .. param)
			end
			url = string.sub(url, 1, start) .. tostring(params[param]) .. string.sub(url, nd, #url)
		end
	until start == nil

	return url
end

---Make an HTTP request
---@param request HTTP_REQUEST
---@return httpTweaked.Response
function httpTweaked.request(request)
	local before = os.epoch("utc")

	http.request(request)

	local event = {}
	repeat
		event = { os.pullEvent() }
	until (event[1] == "http_failure" or event[1] == "http_success") and event[2] == request.url

	local after = os.epoch("utc")

	if event[1] == "http_failure" then
		if event[4] then
			local response = Response.new(request, event[4], after - before)
			logger:error(response:pretty())
			error(response)
		else
			logger:error(event[3])
			error(event[3])
		end
	else
		local response = Response.new(request, event[3], after - before)
		logger:trace(response:pretty())
		return response
	end
end

return httpTweaked
