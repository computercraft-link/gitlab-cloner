local config        = require("ccLogging.config")
local Logger        = require("ccLogging.classes.Logger")
local PrettyHandler = require("ccLogging.classes.PrettyHandler")


local ccLogging = {}


ccLogging.config = config


ccLogging.globalLogger = Logger.new("Global")
ccLogging.globalLogger:setFormat({ long_date = true })
ccLogging.globalLogger:registerHandler(PrettyHandler.new())


ccLogging.error = ccLogging.globalLogger.error
ccLogging.warn  = ccLogging.globalLogger.warn
ccLogging.info  = ccLogging.globalLogger.info
ccLogging.debug = ccLogging.globalLogger.debug
ccLogging.trace = ccLogging.globalLogger.trace

return ccLogging
