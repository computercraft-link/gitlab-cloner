local config        = require("ccLogging.config")
local formatting    = require("ccLogging.util.formatting")

---@class ccLogging.Handler.FileHandler : ccLogging.Handler
---@field private _logFile WriteHandle
local FileHandler   = {}

FileHandler.__index = FileHandler

---Create a new `FileHandler`
---@param path string Path the file should be written to
---@return ccLogging.Handler.FileHandler
function FileHandler.new(path)
    local self = setmetatable({}, FileHandler)

    self.logLevel = config.levels.Trace

    self._logFile = self:_initializeLogFile(path)

    return self
end

---@private
---@param path string
---@return WriteHandle
function FileHandler:_initializeLogFile(path)
    if fs.isReadOnly(path) then
        error("Provided path is read-only", 3)
    end

    if fs.exists(path) then
        fs.delete(path)
    end

    local handle = assert(fs.open(path, "a"))

    return handle --[[@as WriteHandle]]
end

---Logs a log message to the configured file
---@param level ccLogging.levels
---@param format ccLogging.Logger.formatOptions
---@param label string
---@param msg string
function FileHandler:log(level, format, label, msg)
    ---@type string
    local form = ""

    if format.long_date then
        form = form .. formatting.getISOTimestamp() .. " "
    elseif format.unix_timestamp then
        form = form .. formatting.getUNIXTimestamp() .. " "
    end

    form = form .. string.format("[%s] (%s) ", config.levelNames[level] or "???", label)

    self._logFile.writeLine(form .. tostring(msg))
    self._logFile.flush()
end

return FileHandler
