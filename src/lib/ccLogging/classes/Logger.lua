local LOG_LEVELS = require("ccLogging.config").levels

---A log message handler. Should contain a `log` function that receives log messages and writes them to its target location.
---@class ccLogging.Handler
---@field logLevel integer | ccLogging.levels Minimum level to log
---@field log fun(self: ccLogging.Handler, level: ccLogging.levels, format: ccLogging.Logger.formatOptions, label: string, msg: string)

---Flags for formatting
---@class ccLogging.Logger.formatOptions
---@field long_date boolean? Include ISO 8601 date time format
---@field unix_timestamp boolean? Include UNIX timestamp in milliseconds

---@class ccLogging.Logger
---@field label string
---@field handlers ccLogging.Handler[]
---@field private _format ccLogging.Logger.formatOptions
---@field private _parent ccLogging.Logger?
---@field private _children ccLogging.Logger[]
local Logger = {}

Logger.__index = Logger

---Create a new `Logger`
---@param label string
---@return ccLogging.Logger
function Logger.new(label)
    local self = setmetatable({}, Logger)

    self.label = label
    self.handlers = {}
    self._format = { long_date = true }
    self._parent = nil
    self._children = {}

    return self
end

---Create a child `Logger`
---@param label string
---@return ccLogging.Logger
function Logger:createChild(label)
    local child = Logger.new(label)
    child._parent = self

    for _, handler in ipairs(self.handlers) do
        table.insert(child.handlers, handler)
    end

    table.insert(self._children, child)
    return child
end

---Register a log message handler.
---@param handler ccLogging.Handler
---@see ccLogging.Handler
function Logger:registerHandler(handler)
    assert(type(handler) == "table", "handler must be a table")
    assert(type(handler.log) == "function", "handler must contain a log function")
    assert(type(handler.logLevel) == "number", "handler must contain a logLevel")

    table.insert(self.handlers, handler)
end

---Set the format for log messages
---@param format ccLogging.Logger.formatOptions
function Logger:setFormat(format)
    self._format = format
end

---Log a message
---@param level ccLogging.levels Level to log the message at
---@param message any The message to log
function Logger:log(level, message)
    for _, handler in ipairs(self.handlers) do
        if level >= handler.logLevel then
            handler:log(level, self._format, self.label, message)
        end
    end
end

---Log a formatted message. Uses `string.format()`. Note that this does not
---change the formatting of the overall log message, just the formatting of the
---message passed to this function.
---@param level ccLogging.levels
---@param format string The format to apply to the message
---@param message string The message to format and log
function Logger:logf(level, format, message)
    local formattedMessage = string.format(format, message)
    Logger:log(level, formattedMessage)
end

---Log an error message
function Logger:error(message)
    self:log(LOG_LEVELS.Error, message)
end

---Log a warning message
function Logger:warn(message)
    self:log(LOG_LEVELS.Warning, message)
end

---Log an info message
function Logger:info(message)
    self:log(LOG_LEVELS.Info, message)
end

---Log a debug message
function Logger:debug(message)
    self:log(LOG_LEVELS.Debug, message)
end

---Log a trace message
function Logger:trace(message)
    self:log(LOG_LEVELS.Trace, message)
end

return Logger
