local config          = require("ccLogging.config")
local pretty          = require("cc.pretty")
local formatting      = require("ccLogging.util.formatting")

---@class ccLogging.Handler.PrettyHandler : ccLogging.Handler
---@field levelColors table<integer | ccLogging.levels, color>
local PrettyHandler   = {
    levelColors = {
        [config.levels.Error] = colors.red,
        [config.levels.Warning] = colors.yellow,
        [config.levels.Info] = colors.lightBlue,
        [config.levels.Debug] = colors.magenta,
        [config.levels.Trace] = colors.gray
    }
}

PrettyHandler.__index = PrettyHandler

---Create a new `PrettyHandler`
---@return ccLogging.Handler.PrettyHandler
function PrettyHandler.new()
    local self = setmetatable({}, PrettyHandler)

    self.logLevel = config.levels.Info

    return self
end

---Logs a log message using `cc.pretty`
---@param level ccLogging.levels
---@param format ccLogging.Logger.formatOptions
---@param label string
---@param msg string
function PrettyHandler:log(level, format, label, msg)
    ---@type Doc
    local doc

    if format.long_date then
        doc = pretty.text(formatting.getISOTimestamp(), colors.lightGray) .. " "
    elseif format.unix_timestamp then
        doc = pretty.text(tostring(formatting.getUNIXTimestamp()), colors.lightGray) .. " "
    end

    local levelName = config.levelNames[level] or "???"
    local levelColor = self.levelColors[level] or colors.white
    doc = doc .. "[" .. pretty.text(levelName, levelColor) .. "] (" .. pretty.text(label, colors.cyan) .. ") "

    pretty.print(doc .. msg)
end

return PrettyHandler
