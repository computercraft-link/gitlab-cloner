return {
    ---@enum ccLogging.levels
    levels = {
        Error = 6,
        Warning = 3,
        Info = 0,
        Debug = -3,
        Trace = -6
    },
    levelNames = {
        [6] = "ERROR",
        [3] = "WARN",
        [0] = "INFO",
        [-3] = "DEBUG",
        [-6] = "TRACE"
    }
}
