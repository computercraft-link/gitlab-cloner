local formatting = {}

---Get the current date and time in ISO 8601 format
---@return string
function formatting.getISOTimestamp()
    return os.date("!%Y-%m-%dT%H:%M:%SZ") --[[@as string]]
end

---Get the current time as a UNIX timestamp in milliseconds
function formatting.getUNIXTimestamp()
    return os.epoch("utc")
end

return formatting
