local gitlab = require "api.gitlab"
local logging = require "ccLogging"

local logger = logging.globalLogger:createChild("Download Thread")

---@class jobs.Download
---@field projectID integer
---@field branch string
---@field thread thread
---@field attempts integer
---@field rateLimited boolean?
---@field filepath string?
---@field installPath string?
---@field private _runFunction function
local Download = {
    __type = "Download"
}

Download.__index = Download

---Create a new download job
---@param projectID integer
---@param branch string
---@return jobs.Download
function Download.new(projectID, branch)
    local self = setmetatable({}, Download)

    self.attempts = 0
    self.projectID = projectID
    self.branch = branch

    return self
end

function Download:initThread()
    self.attempts = 0

    self.thread = coroutine.create(function()
        while self.attempts < 3 do
            self.attempts = self.attempts + 1
            local success, data = pcall(gitlab.repositories.getRawFile, self.projectID, self.filepath, self.branch) --[[@as any]]

            if success then
                local file = assert(fs.open(self.installPath, "w"))
                file.write(data)
                file.close()
                self.filepath = nil
                self.installPath = nil
                return true
            else
                if data.__type == "Response" then
                    ---@cast data httpTweaked.Response
                    --if rate limited, wait to try again
                    if data.code == 429 then
                        local time = data.headers["retry-after"]
                        local timerID = os.startTimer(tonumber(time) or 30)
                        logger:warn("Rate limited, waiting for " .. time .. " seconds")
                        self.rateLimited = true

                        local event = {}
                        repeat
                            event = { os.pullEvent("timer") }
                        until event[2] == timerID
                        self.rateLimited = false
                    elseif data.code == 404 or data.code == 403 then
                        error(data)
                    end
                else
                    logger:error(data)
                end
            end

            if self.attempts >= 3 then
                error("Max attempts reached")
            end
        end
    end)
end

---Set the file this is downloading
---@param filepath string
---@param installPath string
function Download:setFile(filepath, installPath)
    self.filepath = filepath
    self.installPath = installPath
end

function Download:status()
    return coroutine.status(self.thread)
end

return Download
