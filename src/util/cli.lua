local pretty = require("cc.pretty")

---@class CLI
---@field name string Name of the CLI tool
local CLI = {}

---@class CLI.Flag
---@field required true? Whether this flag is required or not.

---@class CLI.BoolFlag : CLI.Flag
---@field type `CLI.FLAG_TYPE.boolean`

---@class CLI.ValueFlag : CLI.Flag
---@field type `CLI.FLAG_TYPE.string`|`CLI.FLAG_TYPE.number` The data type of this flag.
---@field pattern string? A Lua string pattern that the value should match.

---@class CLI.ShortFlag : CLI.BoolFlag
---@field short true

---@enum CLI.FLAG_TYPE
CLI.FLAG_TYPE = {
	boolean = 0,
	string = 1,
	number = 2,
}

---Registered flags
---@type table<string, CLI.BoolFlag | CLI.ValueFlag | CLI.ShortFlag>
CLI._flags = {}

---Register a flag
---@param name string Name of the flag
---@param type CLI.FLAG_TYPE The data type of this flag
---@param required boolean? Whether this flag is required.
---@param pattern string? A Lua string pattern that the value should match.
function CLI.registerFlag(name, type, required, pattern)
	if CLI._flags[name] then
		error(string.format('A flag with name "%s" already exists', name))
	end

	if type == CLI.FLAG_TYPE.boolean then
		---@type CLI.BoolFlag
		CLI._flags[name] = {
			type = CLI.FLAG_TYPE.boolean,
			required = required or nil,
		}
	else
		---@type CLI.ValueFlag
		CLI._flags[name] = {
			type = type,
			required = required or nil,
			pattern = pattern,
		}
	end
end

---Register a short flag. A short flag should be a single letter and uses only a single hyphen.
---@param name string Name of the flag
---@param required boolean? Whether this flag is required
function CLI.registerShortFlag(name, required)
	if CLI._flags[name] then
		error(string.format('A flag with name "%s" already exists', name))
	end

	---@type CLI.ShortFlag
	CLI._flags[name] = {
		type = CLI.FLAG_TYPE.boolean,
		short = true,
		required = required or nil,
	}
end

---Parse argument flags
---@nodiscard
---@param args table Args passed into script from CLI
---@return table<string, string|boolean|number|nil>?
function CLI.parseFlags(args)
	---@type table<string, string|boolean|number|nil>
	local results = {}

	local argString = table.concat(args, " ")

	for name, data in pairs(CLI._flags) do
		if data.short then
			local match = string.match(argString, "%-" .. name)
			if match then
				results[name] = true
			else
				if data.required then
					error(name .. " argument is required")
					return
				end
			end
		elseif data.type == CLI.FLAG_TYPE.boolean then
			local a = string.find(argString, "--" .. name, nil, true)
			if a then
				results[name] = true
			else
				if data.required then
					error(name .. " argument is required")
					return
				end
			end
		else
			local value = string.match(argString, "%-%-" .. name .. "=?%s?([^%s]+)")
			if value then
				if data.pattern then
					local ok = string.match(value, data.pattern)
					if not ok then
						error(name .. " argument must follow the pattern: " .. data.pattern)
						return
					end
				end
				results[name] = value
			else
				if data.required then
					error(name .. " argument is required")
					return
				end
			end
		end
	end

	return results
end

return CLI
