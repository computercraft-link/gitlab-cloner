local printing = {}

function printing.printLogo()
	local logo = [[
	  /\     /\
	 /  \___/  \
	/           \    GitLab
	\           /    Cloner
	  \       /
	    \   /
	      v
]]
	term.setTextColor(colors.orange)
	print(logo)
	term.setTextColor(colors.white)
end

return printing
